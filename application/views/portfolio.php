<?php include('templates/header.php'); ?>

	<section id="headline_breadcrumbs_bar" class="hadline_no_image">
		<div>
			<div class="container">
				<div class="row">
					<div class="span12 left_aligned headline_title">
						<h2>
							Portfolio 
						</h2>
					</div>
					<span class="social_share white_text">
						<span class="text">Share this Page:</span>
						<a class="share_facebook" href="portfolio-4-columns-full-width.html" title="Share on Facebook"><i class="ci_icon-facebook"></i></a>
						<a class="share_twitter" href="portfolio-4-columns-full-width.html" title="Share on Twitter"><i class="ci_icon-twitter"></i></a>
						<a class="share_email" href="#" title="Share by Email" target="_blank"><i class="ci_icon-email"></i></a>
					</span>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container_fullwidth">
			<ul class="portfolio_filter option-set clearfix" data-option-key="filter">
				<li><a href="#" data-option-value="*" class="portfolio_filter_button selected">All</a></li>
				<li><a href="#" class="portfolio_filter_button" data-option-value=".3d">3D</a></li>
				<li><a href="#" class="portfolio_filter_button" data-option-value=".illustrations">Illustrations</a></li>
				<li><a href="#" class="portfolio_filter_button" data-option-value=".design">Design</a></li>
			</ul>
			<div class="ABdev_latest_portfolio portfolio_items clearfix">
				<div class="portfolio_item portfolio_item_4 3d illustrations">
					<div class="overlayed">
						<img src="assets/images/portfolio11.jpg" alt="portfolio11">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Slice of City</p>
							<p class="portfolio_item_tags">
								3D, Illustrations
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 design">
					<div class="overlayed">
						<img src="assets/images/portfolio3.jpg" alt="portfolio3">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Shopping Bags</p>
							<p class="portfolio_item_tags">
								Design
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 design">
					<div class="overlayed">
						<img src="assets/images/portfolio6.jpg" alt="portfolio6">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Running Field</p>
							<p class="portfolio_item_tags">
								Design
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 design illustrations">
					<div class="overlayed">
						<img src="assets/images/portfolio1.jpg" alt="portfolio1">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Retro Ice Cream – Gallery</p>
							<p class="portfolio_item_tags">
								Design, Illustrations
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 illustrations">
					<div class="overlayed">
						<img src="assets/images/portfolio8.jpg" alt="portfolio8">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Retro Futuristic Car</p>
							<p class="portfolio_item_tags">
								Illustrations
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 3d design">
					<div class="overlayed">
						<img src="assets/images/portfolio5.jpg" alt="portfolio5">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Ram Character</p>
							<p class="portfolio_item_tags">
								3D, Design
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 3d design">
					<div class="overlayed">
						<img src="assets/images/portfolio4.jpg" alt="portfolio4">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Oldman Character</p>
							<p class="portfolio_item_tags">
								3D, Design
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 3d">
					<div class="overlayed">
						<img src="assets/images/portfolio13.jpg" alt="portfolio13">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Nature Hunter</p>
							<p class="portfolio_item_tags">
								3D
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 3d design">
					<div class="overlayed">
						<img src="assets/images/portfolio14.jpg" alt="portfolio14">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Mermaid</p>
							<p class="portfolio_item_tags">
								3D, Design
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 design illustrations">
					<div class="overlayed">
						<img src="assets/images/portfolio9.jpg" alt="portfolio9">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Low Poly Tiger</p>
							<p class="portfolio_item_tags">
								Design, Illustrations
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 illustrations">
					<div class="overlayed">
						<img src="assets/images/portfolio16.jpg" alt="portfolio16">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Like a Sir</p>
							<p class="portfolio_item_tags">
								Illustrations
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 design illustrations">
					<div class="overlayed">
						<img src="assets/images/portfolio10.jpg" alt="portfolio10">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Knowledge Burst</p>
							<p class="portfolio_item_tags">
								Design, Illustrations
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 design">
					<div class="overlayed">
						<img src="assets/images/portfolio7.jpg" alt="portfolio7">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Feel Factory Billboard</p>
							<p class="portfolio_item_tags">
								Design
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 illustrations">
					<div class="overlayed">
						<img src="assets/images/portfolio2.jpg" alt="portfolio2">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Big City Life</p>
							<p class="portfolio_item_tags">
								Illustrations
							</p>
						</a>
					</div>
				</div>
				<div class="portfolio_item portfolio_item_4 design">
					<div class="overlayed">
						<img src="assets/images/portfolio15.jpg" alt="portfolio15">
						<a class="overlay" href="single-portfolio.html">
							<p class="overlay_title">Beauty Blossom</p>
							<p class="portfolio_item_tags">
								Design
							</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="spiral_section_tc blog_subscribe section_bg">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<div class="spiralss_form_wrapper ">
						<form id="spiralss_form_1" class="spiralss_form spiralss_inline_form" action="blog-fullwidth.html" method="post">
							<p>
								<input name="spiralss_subscriber_email" class="spiralss_subscriber_email" placeholder="Enter Your Email Here To Subscribe">
							</p>
							<p>
								<input type="submit" value="Subscribe">
							</p>
							<input type="hidden" name="ajaxnonce" value="55e716029b">
							<input type="hidden" name="formno" value="1">
						</form>
						<div class="spiralss_success_message"></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/footer.php'); ?>