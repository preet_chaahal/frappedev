  <?php include('templates/header.php'); ?>

	<section id="headline_breadcrumbs_bar" class="hadline_no_image">
		<div>
			<div class="container">
				<div class="row">
					<div class="span12 left_aligned headline_title">
						<h2>
							elements
						</h2>
					</div>
					<span class="social_share white_text">
						<span class="text">Share this Page:</span>
						<a class="share_facebook" href="#" title="Share on Facebook"><i class="ci_icon-facebook"></i></a>
						<a class="share_twitter" href="#" title="Share on Twitter"><i class="ci_icon-twitter"></i></a>
						<a class="share_email" href="#" title="Share by Email" target="_blank"><i class="ci_icon-email"></i></a>
					</span>
				</div>
			</div>
		</div>
	</section>
	<section class="spiral_section_tc team_member_section_bg">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span3">
					<div class="spiral_team_member">
						<div class="spiral_overlayed">
							<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">
								<img src="assets/images/team11.jpg" alt="William">
							</a>
							<span class="spiral_team_member_name">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">William Harper</a>
							</span>
							<span class="spiral_team_member_position">CEO / FOUNDER</span>
							<p class="team_content">Donec nibh elit Morbi sem magnad viverra quis sollicitudin quis Nunc sagit tis dapibus nis estibulum dignissim consectetuer nequ Pellentesque vitae purus non elit.</p>
							<div class="spiral_social_links">
								<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Google +" data-gravity="n"><i class="ci_icon-google"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Pinterest" data-gravity="n"><i class="ci_icon-pinterest"></i></a>
							</div>
						</div>
						<div class="spiral_team_member_modal">
							<div class="spiral_container">
								<div class="spiral_column_tc_span5">
									<img src="assets/images/team11.jpg" alt="William">
									<div class="spiral_social_links">
										<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Google +" data-gravity="n"><i class="ci_icon-google"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Pinterest" data-gravity="n"><i class="ci_icon-pinterest"></i></a>
									</div>
								</div>
								<div class="modal_line"></div>
								<div class="spiral_column_tc_span7">
									<h4 class="spiral_team_member_name">
										<span class="first_name">William</span> <span class="last_name">Harper</span>
									</h4>
									<p class="spiral_team_member_position">CEO / FOUNDER</p>
									<div class="content">
										William Harper was appointed CEO in March 2011 after having joined the company as Director in May 2010. He joined Spiral after 14 years at Manhattan Holding Group, one of the shipping world’s largest fuel supply and risk management service providers with an annual turnover of EUR 7 billion.
										<br>
										William holds a Henley Executive MBA from the UK and has worked in Denmark, Holland, Belgium, UK and Australia. Born in the US in 1969, he is married to Sara and has two children, Christian and Anabelle.
										<br>
										<span class="clear spacer_20"></span>
										<h4>
											<span><strong>Key skills</strong></span>
										</h4>
										<span class="clear spacer_20"></span>

										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Risk management</span>

											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_92" data-percentage="92">
													<span>92%</span>
												</div>
											</div>
										</div>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Strategy</span>

											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_86" data-percentage="86">
													<span>86%</span>
												</div>
											</div>
										</div>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Trading</span>

											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_71" data-percentage="71">
													<span>71%</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="spiral_team_member_modal_close"></div>
						</div>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_team_member">
						<div class="spiral_overlayed">
							<a class="spiral_team_member_link" href="#">
								<img src="assets/images/team4.jpg" alt="Sara">
							</a>
							<span class="spiral_team_member_name">
								<a class="spiral_team_member_link" href="#">Sara Jones</a>
							</span>
							<span class="spiral_team_member_position">DEVELOPER</span>
							<p class="team_content">Donec nibh elit Morbi sem magnad viverra quis sollicitudin quis Nunc sagit tis dapibus nis estibulum dignissim consectetuer nequ Pellentesque vitae purus non elit.</p>
							<div class="spiral_social_links">
								<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n"><i class="ci_icon-linkedin"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Google +" data-gravity="n"><i class="ci_icon-google"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Youtube" data-gravity="n"><i class="ci_icon-youtube"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Pinterest" data-gravity="n"><i class="ci_icon-pinterest"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Github" data-gravity="n"><i class="ci_icon-github"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="RSS" data-gravity="n"><i class="ci_icon-rss"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Behance" data-gravity="n"><i class="ci_icon-behance"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_team_member">
						<div class="spiral_overlayed">
							<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">
								<img src="assets/images/team21.jpg" alt="Dave">
							</a>
							<span class="spiral_team_member_name">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">Dave Sergio</a>
							</span>
							<span class="spiral_team_member_position">DESIGNER</span>
							<p class="team_content">Donec nibh elit Morbi sem magnad viverra quis sollicitudin quis Nunc sagit tis dapibus nis estibulum dignissim consectetuer nequ Pellentesque vitae purus non elit.</p>
							<div class="spiral_social_links">
								<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Google +" data-gravity="n"><i class="ci_icon-google"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Pinterest" data-gravity="n"><i class="ci_icon-pinterest"></i></a>
							</div>
						</div>
						<div class="spiral_team_member_modal">
							<div class="spiral_container">
								<div class="spiral_column_tc_span5">
									<img src="assets/images/team21.jpg" alt="Dave">
									<div class="spiral_social_links">
										<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Google +" data-gravity="n"><i class="ci_icon-google"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Pinterest" data-gravity="n"><i class="ci_icon-pinterest"></i></a>
									</div>
								</div>
								<div class="modal_line"></div>
								<div class="spiral_column_tc_span7">
									<h4 class="spiral_team_member_name">
										<span class="first_name">Dave</span> <span class="last_name">Sergio</span>
									</h4>
									<p class="spiral_team_member_position">DESIGNER</p>
									<div class="content">Donec nibh elit Morbi sem magnad viverra quis sollicitudin quis Nunc sagit tis dapibus nis estibulum dignissim consectetuer nequ Pellentesque vitae purus non elit.</div>
								</div>
							</div>
							<div class="spiral_team_member_modal_close"></div>
						</div>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_team_member">
						<div class="spiral_overlayed">
							<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">
								<img src="assets/images/team31.jpg" alt="Andrea">
							</a>
							<span class="spiral_team_member_name">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">Andrea Smith
								</a></span>
							<span class="spiral_team_member_position">ART DIRECTOR</span>

							<p class="team_content">Donec nibh elit Morbi sem magnad viverra quis sollicitudin quis Nunc sagit tis dapibus nis estibulum dignissim consectetuer nequ Pellentesque vitae purus non elit.</p>
							<div class="spiral_social_links">
								<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Google +" data-gravity="n"><i class="ci_icon-google"></i></a>
								<a href="#" target="_self" class="spiral_tooltip" title="Pinterest" data-gravity="n"><i class="ci_icon-pinterest"></i></a>
							</div>
						</div>
						<div class="spiral_team_member_modal">
							<div class="spiral_container">
								<div class="spiral_column_tc_span5">
									<img src="assets/images/team31.jpg" alt="Andrea">
									<div class="spiral_social_links">
										<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Google +" data-gravity="n"><i class="ci_icon-google"></i></a>
										<a href="#" target="_self" class="spiral_tooltip" title="Pinterest" data-gravity="n"><i class="ci_icon-pinterest"></i></a>
									</div>
								</div>
								<div class="modal_line"></div>
								<div class="spiral_column_tc_span7">
									<h4 class="spiral_team_member_name">
										<span class="first_name">Andrea</span> <span class="last_name">Smith</span>
									</h4>
									<p class="spiral_team_member_position">ART DIRECTOR</p>
									<div class="content">Donec nibh elit Morbi sem magnad viverra quis sollicitudin quis Nunc sagit tis dapibus nis estibulum dignissim consectetuer nequ Pellentesque vitae purus non elit.</div>
								</div>
							</div>
							<div class="spiral_team_member_modal_close"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
    <?php include('templates/footer.php'); ?>