   <?php include('templates/header.php'); ?>

	<section id="headline_breadcrumbs_bar" class="hadline_no_image">
		<div>
			<div class="container">
				<div class="row">
					<div class="span12 left_aligned headline_title">
						<h2>
							Blog
						</h2>
					</div>
					<span class="social_share white_text">
						<span class="text">Share this Page:</span>
						<a class="share_facebook" href="blog-timeline.html" title="Share on Facebook"><i class="ci_icon-facebook"></i></a>
						<a class="share_twitter" href="blog-timeline.html" title="Share on Twitter"><i class="ci_icon-twitter"></i></a>
						<a class="share_email" href="#" title="Share by Email" target="_blank"><i class="ci_icon-email"></i></a>
					</span>
				</div>
			</div>
		</div>
	</section>
	<section class="blog_section">
		<div class="container">
			<div id="timeline_posts" class="clearfix">
				<div class="timeline_post timeline_post_first has-post-thumbnail">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Jun</span>
							<span class="post_main_date">15</span>
						</div>
					</div>
					<div class="row">
						<div class="span6">
							<img src="assets/images/post2.jpg" alt="post2">
						</div>
						<div class="span6">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">This is an awesome title of blog post</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a>
										</span> |  
										<span>
											<a href="#" class="scroll comments_link">1 comment</a>
										</span> | 
										<span class="category">
											<a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post has-post-thumbnail">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Jun</span>
							<span class="post_main_date">15</span>
						</div>
					</div>
					<div class="row">
						<div class="span6">
							<div class="videoWrapper-youtube">
								<iframe src="http://www.youtube.com/embed/Mze-piXz7OE?autoplay=0&amp;modestbranding=0&amp;controls=0&amp;fs=0&amp;start=&amp;end=&amp;showinfo=0&amp;rel=0" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<div class="span6">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">Post with youtube video</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Mar</span>
							<span class="post_main_date">14</span>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-no-sidebar-no-image.html">Regular post without image</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post tag-fresh tag-modern tag-new">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Feb</span>
							<span class="post_main_date">15</span>
						</div>
					</div>
					<div class="row">
						<div class="span6">
							<div class="videoWrapper-vimeo">
								<iframe src="http://player.vimeo.com/video/128877443?title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</div>
						</div>
						<div class="span6">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">Another cool Vimeo video</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										Your bones don’t break, mine do. Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post has-post-thumbnail">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Jan</span>
							<span class="post_main_date">15</span>
						</div>
					</div>
					<div class="row">
						<div class="span6">
							<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F127716067"></iframe>	
						</div>
						<div class="span6">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">Audio post example</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post has-post-thumbnail">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Nov</span>
							<span class="post_main_date">15</span>
						</div>
					</div>
					<div class="row">
						<div class="span6">
							<img src="assets/images/post3.jpg" alt="post3">
						</div>
						<div class="span6">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">So you want to know more?</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Oct</span>
							<span class="post_main_date">28</span>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">Regular plog post</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										Your bones don’t break, mine do. Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post has-post-thumbnail">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Oct</span>
							<span class="post_main_date">16</span>
						</div>
					</div>
					<div class="row">
						<div class="span6">
							<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F127716067"></iframe>	
						</div>
						<div class="span6">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">Post with soundcloud audio</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post has-post-thumbnail">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Aug</span>
							<span class="post_main_date">15</span>
						</div>
					</div>
					<div class="row">
						<div class="span6">
							<img src="assets/images/post4.jpg" alt="post4">
						</div>
						<div class="span6">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">Spiral is full of surprises</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										Cities fall but they are rebuilt. Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="timeline_post">
					<div class="post_info">
						<div class="post_date">
							<span class="post_main_month">Jul</span>
							<span class="post_main_date">15</span>
						</div>
					</div>
					<div class="row">
						<div class="span12">
							<div class="post_main_inner_wrapper">
								<h2>
									<a href="single-blog-fullwidth.html">Only the best post laying around</a>
								</h2>
								<div class="timeline_postmeta">
									<span class="post_author">
										by 
										<span>
											<a href="#" title="Posts by admin" rel="author">admin</a></span> |  
										<span>
											<a href="#" class="scroll comments_link">No comments</a>
										</span> | 
										<span class="category"><a href="blog-right-sidebar.html">Events</a>, 
											<a href="blog-fullwidth.html">Fullwidth Blog</a>, 
											<a href="blog-left-sidebar.html">Left Sidebar Blog</a>, 
											<a href="blog-timeline.html">Timeline Blog</a>
										</span>
									</span>
								</div>
								<div class="timeline_content">
									<p>
										Do you see any Teletubbies in here? Proin gravida nibh vel velit auctor aliquet.
										<br>
										Now that we know who you are, I know who I am. I’m not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain’s going to be? He’s the exact opposite of the hero.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="timeline_loading" data-ajaxurl="timeline-posts.html" data-noposts="No older posts found">
				No older posts found
			</div>
		</div>
	</section>
	<section class="spiral_section_tc blog_subscribe section_bg">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<div class="spiralss_form_wrapper ">
						<form id="spiralss_form_1" class="spiralss_form spiralss_inline_form" action="blog-fullwidth.html" method="post">
							<p>
								<input name="spiralss_subscriber_email" class="spiralss_subscriber_email" placeholder="Enter Your Email Here To Subscribe">
							</p>
							<p>
								<input type="submit" value="Subscribe">
							</p>
							<input type="hidden" name="ajaxnonce" value="55e716029b">
							<input type="hidden" name="formno" value="1">
						</form>
						<div class="spiralss_success_message"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php include('templates/footer.php'); ?>