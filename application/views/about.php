
	<?php include('templates/header.php'); ?>

	<section id="headline_breadcrumbs_bar" class="with_image">
		<div class="headline_image headline_style_about_us">
			<div class="container">
				<div class="row">
					<div class="span12 left_aligned headline_title">
						<h2>About us</h2>
					</div>
					<h3 class="white_text page_meta_description">
						This is a short description of this page.
					</h3>
					<div id="button_social" class="headline_button">
						<span class="social_button_title">Share this Page</span>
						<span class="social_button_contents">
							<a href="about-us.html" title="Share on Facebook"><i class="ci_icon-facebook"></i></a>
							<a href="about-us.html" title="Share on Twitter"><i class="ci_icon-twitter"></i></a>
							<a href="#" title="Share by Email" target="_blank"><i class="ci_icon-email"></i></a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="who_we_are" class="spiral_section_tc section_with_header no_padding_bottom">
		<header>
			<div class="spiral_container">
				<h3>
					Who <strong>We</strong> Are
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span6">
					<h2 class="gray_text no_margin_top">
						<span>
							Eventually everything connects –
							<br>
							people, ideas, shapes. The quality
							<br>
							of the connections is the key to
							<br>
							quality per se.
						</span>
					</h2>
					<span class="clear spacer_25"></span>
					<div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima, minus iure accusantium dolores, officiis eveniet architecto error placeat ratione incidunt?</p>
					</div>
					<div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima, minus iure accusantium dolores, officiis eveniet architecto error placeat ratione incidunt?</p>
					</div>
				</div>
				<div class="spiral_column_tc_span6">
					<div class="spiral-accordion " data-expanded="1">
						<h3>
							Vision of Spiral
						</h3>
						<div class="spiral-accordion-body">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab vel mollitia dignissimos cupiditate rerum facilis similique vero possimus, dolor, voluptatibus minus enim aspernatur. Vel, molestias aut.
						</div>
						<h3>
							Our Mission
						</h3>
						<div class="spiral-accordion-body">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab vel mollitia dignissimos cupiditate rerum facilis similique vero possimus, dolor, voluptatibus minus enim aspernatur. Vel, molestias aut.
						</div>
						<h3>
							Strategic Goals
						</h3>
						<div class="spiral-accordion-body">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab vel mollitia dignissimos cupiditate rerum facilis similique vero possimus, dolor, voluptatibus minus enim aspernatur. Vel, molestias aut.
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="spiral_section_tc">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span4">
					<div class="spiral_team_member">
						<div class="spiral_overlayed">
							<a class="spiral_team_member_link spiral_team_member_modal_link" href="about-us.html">
								<img src="assets/images/team3.jpg" alt="Angus">
							</a>
							<span class="spiral_team_member_name">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="about-us.html">Angus Cameron</a>
							</span>
							<span class="spiral_team_member_position">Head of Office</span>
							<p class="team_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<div class="spiral_social_links">
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n"><i class="ci_icon-linkedin"></i></a>
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
							</div>
						</div>
						<div class="spiral_team_member_modal">
							<div class="spiral_container">
								<div class="spiral_column_tc_span5">
									<img src="assets/images/team3.jpg" alt="Angus">
									<div class="spiral_social_links">
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n"><i class="ci_icon-linkedin"></i></a>
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
									</div>
								</div>
								<div class="modal_line"></div>
								<div class="spiral_column_tc_span7">
									<h4 class="spiral_team_member_name">
										<span class="first_name">Angus</span> <span class="last_name">Cameron</span>
									</h4>
									<p class="spiral_team_member_position">Head of Office</p>
									<div class="content">
										Angus Cameron was appointed CEO in March 2011 after having joined the company as Director in May 2010. He joined Spiral after 14 years at Manhattan Holding Group, one of the shipping world’s largest fuel supply and risk management service providers with an annual turnover of EUR 7 billion.
										<br>
										Angus holds a Henley Executive MBA from the UK and has worked in Denmark, Holland, Belgium, UK and Australia. Born in the US in 1969, he is married to Sara and has two children, Christian and Anabelle.
										<h3>
											<span>Key skills</span>
										</h3>
										<span class="clear spacer_20"></span>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Risk management</span>
											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_92" data-percentage="92">
													<span>92%</span>
												</div>
											</div>
										</div>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Strategy</span>
											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_86" data-percentage="86">
													<span>86%</span>
												</div>
											</div>
										</div>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Trading</span>
											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_71" data-percentage="71">
													<span>71%</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="spiral_team_member_modal_close"></div>
						</div>
					</div>
				</div>
				<div class="spiral_column_tc_span4">
					<div class="spiral_team_member">
						<div class="spiral_overlayed">
							<a class="spiral_team_member_link spiral_team_member_modal_link" href="about-us.html">
								<img src="assets/images/team2.jpg" alt="Mary M.">
							</a>
							<span class="spiral_team_member_name">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="about-us.html">Mary M. Smith</a>
							</span>
							<span class="spiral_team_member_position">Designer</span>
							<p class="team_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<div class="spiral_social_links">
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n"><i class="ci_icon-linkedin"></i></a>
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
							</div>
						</div>
						<div class="spiral_team_member_modal">
							<div class="spiral_container">
								<div class="spiral_column_tc_span5">
									<img src="assets/images/team2.jpg" alt="Mary M.">
									<div class="spiral_social_links">
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n"><i class="ci_icon-linkedin"></i></a>
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
									</div>
								</div>
								<div class="modal_line"></div>
								<div class="spiral_column_tc_span7">
									<h4 class="spiral_team_member_name">
										<span class="first_name">Mary M.</span> <span class="last_name">Smith</span>
									</h4>
									<p class="spiral_team_member_position">Designer</p>
									<div class="content">
										Earned praised for my work investing in salsa in Mexico. Spent 2001-2004 marketing Roombas on the black market. Spent a weekend building puppets in Orlando, FL. Had moderate success developing strategies for magma in Ohio. At the moment I’m short selling inflatable dolls in Miami, FL. Spent childhood writing about jungle gyms in Las Vegas, NV.
										<h3>
											<span>Key skills</span>
										</h3>
										<span class="clear spacer_20"></span>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Risk management</span>

											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_92" data-percentage="92">
													<span>92%</span>
												</div>
											</div>
										</div>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Strategy</span>
											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_86" data-percentage="86">
													<span>86%</span>
												</div>
											</div>
										</div>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Trading</span>
											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_71" data-percentage="71">
													<span>71%</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="spiral_team_member_modal_close"></div>
						</div>
					</div>
				</div>
				<div class="spiral_column_tc_span4">
					<div class="spiral_team_member">
						<div class="spiral_overlayed">
							<a class="spiral_team_member_link spiral_team_member_modal_link" href="about-us.html">
								<img src="assets/images/team1.jpg" alt="Samuel">
							</a>
							<span class="spiral_team_member_name">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="about-us.html">Samuel Gilmore</a>
							</span>
							<span class="spiral_team_member_position">Public Relations</span>
							<p class="team_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<div class="spiral_social_links">
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n"><i class="ci_icon-linkedin"></i></a>
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
								<a href="about-us.html" target="_self" class="spiral_tooltip" title="Vimeo" data-gravity="n"><i class="ci_icon-vimeo"></i></a>
							</div>
						</div>
						<div class="spiral_team_member_modal">
							<div class="spiral_container">
								<div class="spiral_column_tc_span5">
									<img src="assets/images/team1.jpg" alt="Samuel">
									<div class="spiral_social_links">
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n"><i class="ci_icon-twitter"></i></a>
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n"><i class="ci_icon-linkedin"></i></a>
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n"><i class="ci_icon-facebook"></i></a>
										<a href="about-us.html" target="_self" class="spiral_tooltip" title="Vimeo" data-gravity="n"><i class="ci_icon-vimeo"></i></a>
									</div>
								</div>
								<div class="modal_line"></div>
								<div class="spiral_column_tc_span7">
									<h4 class="spiral_team_member_name">
										<span class="first_name">Samuel</span> <span class="last_name">Gilmore</span>
									</h4>
									<p class="spiral_team_member_position">Public Relations</p>
									<div class="content">
										Won several awards for analyzing lint in Fort Walton Beach, FL. Spent high school summers marketing foreign currency in Orlando, FL. Spent a year training race cars on Wall Street. Spoke at an international conference about lecturing about mannequins in Africa. At the moment I’m donating wooden horses in Hanford, CA. My current pet project is importing action figures in Las Vegas, NV.
										<h3>
											<span>Key skills</span>
										</h3>
										<span class="clear spacer_20"></span>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Risk management</span>
											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_92" data-percentage="92">
													<span>92%</span>
												</div>
											</div>
										</div>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Strategy</span>
											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_86" data-percentage="86">
													<span>86%</span>
												</div>
											</div>
										</div>
										<div class="spiral_progress_bar spiral_progress_bar_default">
											<span class="spiral_meter_label">Trading</span>
											<div class="spiral_meter">
												<div class="spiral_meter_percentage meter_percentage_width_71" data-percentage="71">
													<span>71%</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="spiral_team_member_modal_close"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="spiral_section_tc">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<div class="spiral-tabs spiral-tabs-timeline" data-selected="3" data-break_point="368" data-effect="slide">
						<ul class="nav-tabs">
							<li><a class="spiral-tabs-tab" data-href="#tab-1-1" href="#">2000</a></li>
							<li><a class="spiral-tabs-tab" data-href="#tab-1-2" href="#">2003</a></li>
							<li class="active"><a class="spiral-tabs-tab" data-href="#tab-1-3" href="#">2005</a></li>
							<li><a class="spiral-tabs-tab" data-href="#tab-1-4" href="#">2009</a></li>
							<li><a class="spiral-tabs-tab" data-href="#tab-1-5" href="#">2014</a></li>
						</ul>
						<div class="tab-content ">
							<div id="tab-1-1" class="tab-pane">
								<p>We understand the process of building websites and mobile applications through our expertise and produce award winning solutions that become popular and make our clients achieve their goals and values.</p>
							</div>
							<div id="tab-1-2" class="tab-pane">
								<p>We understand the process of building websites and mobile applications through our expertise and produce award winning solutions that become popular and make our clients achieve their goals and values.</p>
							</div>
							<div id="tab-1-3" class="tab-pane active_pane">
								<p>We understand the process of building websites and mobile applications through our expertise and produce award winning solutions that become popular and make our clients achieve their goals and values.</p>
							</div>
							<div id="tab-1-4" class="tab-pane">
								<p>We understand the process of building websites and mobile applications through our expertise and produce award winning solutions that become popular and make our clients achieve their goals and values.</p>
							</div>
							<div id="tab-1-5" class="tab-pane">
								<p>We understand the process of building websites and mobile applications through our expertise and produce award winning solutions that become popular and make our clients achieve their goals and values.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="spiral_section_tc column_title_left section_with_header no_margin_top">
		<header>
			<div class="spiral_container">
				<h3>
					But don’t take our <strong>Word</strong> for it
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span5">
					<div class="spiralt_testimonials_wrapper picture_bottom">
						<ul class="spiralt_testimonials_slide" data-play="1" data-fx="fade" data-easing="linear" data-direction="left" data-duration="1000" data-pauseonhover="immediate" data-timeoutduration="5000">
							<li class="testimonials_item">
								<div class="testimonial_small">
									<p>Love the functionality of the theme, it's very versatile, Try it out, you won't regret it.</p>
									<img src="assets/images/testim1.jpg" alt="testim1">
									<span class="source"><span class="spiralt_author">Octavio Readmay</span> <span class="spiralt_company">Lolipop Inc.</span></span>
								</div>
							</li>
							<li class="testimonials_item">
								<div class="testimonial_small">
									<p>There are a lot of cool features in this template. You should definitely buy it and check them out!</p>
									<img src="assets/images/testim2.jpg" alt="testim2">
									<span class="source"><span class="spiralt_author">Emmaline Blust</span> <span class="spiralt_company">Hightech Enterprise</span></span>
								</div>
							</li>
							<li class="testimonials_item">
								<div class="testimonial_small">
									<p>Writing something instead of nothing is a good way to get noticed. Even in the simplest things.</p>
									<img width="292" height="292" src="assets/images/team21.jpg" alt="team2">
									<span class="source"><span class="spiralt_author">Rufus Huppert</span> <span class="spiralt_company">Happy Inc.</span></span>
								</div>
							</li>
							<li class="testimonials_item">
								<div class="testimonial_small">
									<p>Good luck and keep it up! I have never seen such a great quality. Now I’m going to rate this item 5 stars. Because it’s awesome.</p>
									<img src="assets/images/testim4.jpg" alt="testim4">
									<span class="source"><span class="spiralt_author">Gena Brehmer</span> <span class="spiralt_company">Company Ltd.</span></span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="spiral_column_tc_span1 responsive_hide"></div>
				<div class="spiral_column_tc_span2">
					<div class="spiral-animo">
						<a href="about-us.html">
							<img src="assets/images/1.png" alt="">
						</a>
					</div>
					<span class="clear spacer_30"></span>
					<div class="spiral-animo">
						<a href="about-us.html">
							<img src="assets/images/4.png" alt="">
						</a>
					</div>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral-animo">
						<a href="about-us.html">
							<img src="assets/images/2.png" alt="">
						</a>
					</div>
					<span class="clear spacer_30"></span>
					<div class="spiral-animo">
						<a href="about-us.html">
							<img src="assets/images/5.png" alt="">
						</a>
					</div>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral-animo">
						<a href="about-us.html">
							<img src="assets/images/3.png" alt="">
						</a>
					</div>
					<span class="clear spacer_30"></span>
					<div class="spiral-animo">
						<a href="about-us.html">
							<img src=assets/images/6.png" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="spiral_section_tc about_us_section_bg">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span4">
					<h3 class="white_text big_text">
						<span>Some <strong>Fun</strong> Facts</span>
					</h3>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral_stats_excerpt spiral_stats_excerpt_1">
						<i class="tmf-medall stats_excerpt_1_icon_color"></i>
						<span class="spiral_stats_number stats_number_color_white" data-number="74" data-duration="1500"></span> 
						<p class="stats_number_color_white">Awards won</p>
					</div>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral_stats_excerpt spiral_stats_excerpt_1">
						<i class="tmf-game stats_excerpt_1_icon_color"></i>
						<span class="spiral_stats_number stats_number_color_white" data-number="3" data-duration="1500"></span> 
						<p class="stats_number_color_white">Serious Gamers</p>
					</div>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral_stats_excerpt spiral_stats_excerpt_1">
						<i class="tmf-receipt stats_excerpt_1_icon_color"></i>
						<span class="spiral_stats_number stats_number_color_white" data-number="96" data-duration="1500"></span>
						<span class="spiral_stats_number_sign stats_number_color_white">k</span>
						<p class="stats_number_color_white">Lines of Code</p>
					</div>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral_stats_excerpt spiral_stats_excerpt_1">
						<i class="tmf-package stats_excerpt_1_icon_color"></i>
						<span class="spiral_stats_number stats_number_color_white" data-number="180" data-duration="1500"></span> 
						<p class="stats_number_color_white">Projects Done</p>
					</div>
				</div>
			</div>
		</div>
	</section>

		<?php include('templates/footer.php'); ?>