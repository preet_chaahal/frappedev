<footer id="ABdev_main_footer">
		<div id="footer_copyright">
			<div class="container">
				<div class="row">
					<div class="span4 footer_copyright left_aligned">
						© 2016. All Rights Reserved				
					</div>
					<div class="span4 center_aligned">
						<a href="#" id="back_to_top" title="Back to top">
							<i class="ci_icon-angle-up"></i>
						</a>
					</div>
					<div class="span4 footer_credits right_aligned">
						Developed By Koffeeion				
					</div>
				</div>
			</div>
		</div>
	</footer>
	
	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="assets/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="assets/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="assets/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="assets/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="assets/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="assets/js/prettify.js"></script>
	<script type="text/javascript" src="assets/http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="assets/js/portfolio-init.js"></script>
	<script type="text/javascript" src="assets/js/scripts.js"></script>
	<script type="text/javascript" src="assets/js/custom.js"></script>
	
</body>
</html>