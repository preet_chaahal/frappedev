<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Home | Spiral</title>
	<meta name="description" content="Spiral HTML5 premium template">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png">
	<!--[if lt IE 9]>
	  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C400italic%2C300%2C700%7CRaleway%3A400%2C200%2C600%2C700&amp;ver=4.2.6" type="text/css" media="all">
	<link rel="stylesheet" href="assets/rs-plugin/css/settings.css" type="text/css" media="all">
	<link rel="stylesheet" href="assets/css/icons/icons.css" type="text/css" media="all">
	<link rel="stylesheet" href="assets/css/core-icons/core_style.css" type="text/css" media="all">
	<link rel="stylesheet" href="assets/css/scripts.css" type="text/css" media="all">
	<link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all">
</head>

<body class="page">
<header id="ABdev_main_header" class="clearfix ">
		<div class="container clearfix">
			<div id="logo">
				<a href="index.html">
					<img id="main_logo" src="assets/images/logomakr.png" alt="FrappeDev">
				</a>
			</div>
			<div class="menu_wrapper">
				<div class="menu_slide_toggle">
					<div class="icon-menu"></div>
				</div>
				<div id="title_breadcrumbs_bar">
					<div class="breadcrumbs">
						<a href="home.php">Home</a>
						<i class="ci_icon-angle-right"></i>
						<span class="current">Home</span>
					</div>
				</div>
				<nav>
					<ul>
						<li class="current-menu-item"><a href="<?php echo base_url(); ?>"><span>Home</span></a></li>
						<li>
							<a href="<?php echo base_url('about'); ?>" class="scroll"><span>About Us</span></a>
						</li>
						<li>
							<a href="<?php echo base_url('portfolio'); ?>" class="scroll"><span>Our Works</span></a>
						</li>
						<li>
							<a href="<?php echo base_url('clients'); ?>" class="scroll"><span>Clients</span></a>
						</li>
						<li>
							<a href="<?php echo base_url('training'); ?>" class="scroll"><span>Training</span></a>
						</li>
						<li>
							<a href="<?php echo base_url('contact'); ?>" class="scroll"><span>Contact</span></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
