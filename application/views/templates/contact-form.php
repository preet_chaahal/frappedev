	<section class="spiral_section_tc column_title_left section_with_header">
		<header>
			<div class="spiral_container">
				<h3>Get in 
					<strong>Touch</strong>
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span4">
					<h3 class="gray_text">
						<span>Communication – the<br>human connection<br>– is the key to personal<br>and career success.”</span>
					</h3>
					<span class="clear spacer_30"></span>
					<div class="gray_text">
						<p>3285/2 first floor<br>New Tagore Nagar, Haibowal Kalan <br> Ludiana</p>
						<p>+91 161 504-7879<br>
							<a href="mailto:">info@frappedev.com</a>
						</p>
					</div>
				</div>
				<div class="spiral_column_tc_span8">
					<h2 class="dark_gray">
						<span>
							<strong>Say hello!</strong>
						</span>
					</h2>
					<span class="clear spacer_10"></span>
					<div class="spiralcf" id="spiralcf-wrapper" dir="ltr">
						<form action="#" method="post" class="contact-form">
							<div class="hidden">
								<input type="hidden" name="nonce" value="214a162653">
								<input type="hidden" name="formid" id="formid" value="contact">
							</div>
							<div class="row">
								<div class="span6">
									<span class="spiralcf-form-control-wrap your-name">
										<input type="text" name="name" value="" size="40" class="spiralcf-text" placeholder="Name">
									</span>
								</div>
								<div class="span6">
									<span class="spiralcf-form-control-wrap your-email">
										<input type="email" name="email" value="" size="40" class="spiralcf-text spiralcf-email spiralcf-validates-as-email" placeholder="Email">
									</span>
								</div>
							</div>
							<div class="row">
								<span class="spiralcf-form-control-wrap your-message">
									<textarea name="message" cols="40" rows="10" class="spiralcf-textarea" placeholder="Message"></textarea>
								</span>
								<br>
								<input type="submit" value="Send Message" class="spiralcf-submit"  id="spiralcf-submit">
							</div>
							<div class="spiralcf-response-output spiralcf-display-none"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="spiral_section_tc section_body_fullwidth no_padding">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3422.813903172969!2d75.81710101474269!3d30.91982548157237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391a83efedfd8aed%3A0x2fee4423ea01682a!2s3285%2C+New+Tagore+Nagar%2C+Haibowal+Kalan%2C+Ludhiana%2C+Punjab+141001!5e0!3m2!1sen!2sin!4v1490773245655" width="1500" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						
					</div>
				</div>
			</div>
		</div>
	</section>