
	<?php include('templates/header.php'); ?>

	<section id="ABdev_main_slider">
		<div id="rev_slider_12_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="main_slider" style="background-color:#E9E9E9;padding:0px;">
			<!-- START REVOLUTION SLIDER 5.1.4 fullscreen mode -->
			<div id="rev_slider_12_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.1.4">
				<ul>	
					<!-- SLIDE  -->
					<li data-index="rs-38" data-transition="slideup" data-slotamount="0"  data-easein="default" data-easeout="default" data-masterspeed="500"  data-thumb="assets/rs-plugin/assets/1bg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Leaf Girl" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
						<!-- MAIN IMAGE -->
						<img src="assets/rs-plugin/assets/dummy.png"  alt=""  data-lazyload="assets/rs-plugin/assets/1bg.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="rev-slidebg" data-no-retina>
						<!-- LAYERS -->
						<!-- LAYER NR. 1 -->
						<div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-38-layer-1" data-x="-150" data-y="-200" data-width="auto" data-height="auto" data-transform_idle="" data-transform_in="x:900;y:0;z:0;rX:20;rY:10;rZ:-50;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" data-transform_out="x:900;y:0;z:0;rX:20;rY:10;rZ:-50;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;s:1000;" data-start="0" data-responsive_offset="on" style="z-index: 5;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="" width="1279" height="1595" data-lazyload="assets/rs-plugin/assets/1a.png" data-no-retina> 
						</div>
						<!-- LAYER NR. 2 -->
						<div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-38-layer-2" data-x="right" data-hoffset="-500" data-y="-80" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:right;s:2500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="150" data-responsive_offset="on" style="z-index: 6;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="" width="1180" height="1235" data-lazyload="assets/rs-plugin/assets/1b.png" data-no-retina> 
						</div>
						<!-- LAYER NR. 3 -->
						<div class="tp-caption   tp-resizeme rs-parallaxlevel-3" id="slide-38-layer-3" data-x="-80" data-y="-214" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:900;y:0;z:0;rX:20;rY:10;rZ:-50;sX:1;sY:1;skX:0;skY:0;opacity:0;s:3000;e:Power3.easeInOut;" data-transform_out="x:900;y:0;z:0;rX:20;rY:10;rZ:-50;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;s:1000;" data-start="100" data-responsive_offset="on" style="z-index: 7;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="" width="1742" height="1436" data-lazyload="assets/rs-plugin/assets/1c.png" data-no-retina> 
						</div>
						<!-- LAYER NR. 4 -->
						<div class="tp-caption Raleway   tp-resizeme" id="slide-38-layer-4" data-x="-19" data-y="437" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 8; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);">
							<strong>SPIRAL</strong>
							IS WHAT<br>
							MAKES YOUR IDEA GO LIVE 
						</div>
						<!-- LAYER NR. 5 -->
						<div class="tp-caption Raleway_small   tp-resizeme" id="slide-38-layer-5" data-x="-21" data-y="660" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1600;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="550" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-captionhidden="on" style="z-index: 9; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);">Intuitive Drag and Drop builder, a plethora of elements to build with,<br>
							and even more options to tweak the final result. 
						</div>
						<!-- LAYER NR. 6 -->
						<div class="tp-caption none   tp-resizeme" id="slide-38-layer-6" data-x="6" data-y="806" data-width="auto" data-height="auto" data-transform_idle="" data-transform_in="y:50px;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="1600" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 10; white-space: nowrap;border-color:rgba(34, 34, 34, 1.00);">
							<a href='#'  class="rev_slider_button_red">Purchase Now</a> 
						</div>
						<!-- LAYER NR. 7 -->
						<div class="tp-caption none   tp-resizeme" id="slide-38-layer-7" data-x="200" data-y="806" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:50px;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="1600" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-captionhidden="on" style="z-index: 11; white-space: nowrap;border-color:rgba(34, 34, 34, 1.00);">
							<a href='#'  class="rev_slider_button_white">Or View Options</a> 
						</div>
					</li>
					<!-- SLIDE  -->
					<li data-index="rs-40" data-transition="" data-slotamount="7"  data-easein="default" data-easeout="default" data-thumb=""  data-rotate="0"  data-saveperformance="off"  data-title="Video" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
						<!-- MAIN IMAGE -->
						<img src="assets/rs-plugin/assets/dummy.png"  alt=""  data-lazyload="assets/rs-plugin/assets/transparent.png" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="rev-slidebg" data-no-retina>
						<!-- LAYERS -->
						<!-- LAYER NR. 1 -->
						<div class="tp-caption   tp-resizeme fullscreenvideo tp-videolayer" id="slide-40-layer-1" data-x="0" data-y="0" data-transform_idle="" data-transform_in="y:bottom;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:500;" data-start="0" data-responsive_offset="on" data-videocontrols="none" data-videowidth="100%" data-videoheight="100%" data-videoposter="assets/rs-plugin/assets/Leaf.jpg" data-videoogv="assets/rs-plugin/assets/Leaf.ogv" data-videowebm="assets/rs-plugin/assets/Leaf.webm" data-videomp4="assets/rs-plugin/assets/Leaf.mp4" data-noposteronmobile="off" data-videopreload="auto" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9" data-autoplay="on" data-nextslideatend="true" style="z-index: 5;"> 
						</div>
						<!-- LAYER NR. 2 -->
						<div class="tp-caption   tp-resizeme" id="slide-40-layer-2" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:500;" data-start="0" data-responsive_offset="on" style="z-index: 6;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="" width="5" height="5" data-ww="3000" data-hh="3000" data-lazyload="assets/rs-plugin/assets/overlay.png" data-no-retina> 
						</div>
						<!-- LAYER NR. 3 -->
						<div class="tp-caption Raleway   tp-resizeme" id="slide-40-layer-3" data-x="center" data-hoffset="0" data-y="center" data-voffset="50" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1;s:1;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);">
							<div style="text-align: center;">
								<strong>CREATIVITY</strong>
								COMES FROM<br>
								A CONFLICT OF IDEAS
							</div> 
						</div>
						<!-- LAYER NR. 4 -->
						<div class="tp-caption Raleway_small   tp-resizeme" id="slide-40-layer-4" data-x="center" data-hoffset="0" data-y="center" data-voffset="215" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1600;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1;s:1;" data-start="550" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 8; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);">
							<div style="text-align: center;">We kept it simple, while considering each scenario<br>
								so now it can handle any kind of content and still look great!
							</div> 
						</div>
						<!-- LAYER NR. 5 -->
						<div class="tp-caption none   tp-resizeme" id="slide-40-layer-5" data-x="410" data-y="812" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:50px;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1;s:1;" data-start="1600" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 9; white-space: nowrap;border-color:rgba(34, 34, 34, 1.00);">
							<a href='#'  class="rev_slider_button_red">Purchase Now</a> 
						</div>
						<!-- LAYER NR. 6 -->
						<div class="tp-caption none   tp-resizeme" id="slide-40-layer-6" data-x="612" data-y="812" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:50px;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1;s:1;" data-start="1600" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-captionhidden="on" style="z-index: 10; white-space: nowrap;border-color:rgba(34, 34, 34, 1.00);">
							<a href='#'  class="rev_slider_button_white">or View Features </a> 
						</div>
					</li>
					<!-- SLIDE  -->
					<li data-index="rs-41" data-transition="slidedown" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-rotate="0"  data-saveperformance="on"  data-title="Devices Desk" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
						<!-- MAIN IMAGE -->
						<img src="assets/rs-plugin/assets/dummy.png" style='background-color:#5A5A5A' alt=""  data-lazyload="assets/rs-plugin/assets/transparent.png" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="rev-slidebg" data-no-retina>
						<!-- LAYERS -->
						<!-- LAYER NR. 1 -->
						<div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-1" data-x="center" data-hoffset="0" data-y="-150" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:top;s:1250;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="500" data-responsive_offset="on" style="z-index: 5;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="" width="1237" height="614" data-lazyload="assets/rs-plugin/assets/3b1.jpg" data-no-retina> 
						</div>
						<!-- LAYER NR. 2 -->
						<div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-2" data-x="1086" data-y="310" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:right;s:1450;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="550" data-responsive_offset="on" style="z-index: 6;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="" width="310" height="416" data-lazyload="assets/rs-plugin/assets/3c.jpg" data-no-retina> 
						</div>
						<!-- LAYER NR. 3 -->
						<div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-3" data-x="821" data-y="587" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:right;s:1400;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="500" data-responsive_offset="on" style="z-index: 7;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="" width="240" height="263" data-lazyload="assets/rs-plugin/assets/3d.jpg" data-no-retina> 
						</div>
						<!-- LAYER NR. 4 -->
						<div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-4" data-x="-300" data-y="center" data-voffset="0" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:left;s:1500;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="580" data-responsive_offset="on" style="z-index: 8;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="" width="589" height="621" data-lazyload="assets/rs-plugin/assets/3a.png" data-no-retina> 
						</div>
						<!-- LAYER NR. 5 -->
						<div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-5" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="143" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1000;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="625" data-responsive_offset="on" style="z-index: 9;">
							<img src="assets/rs-plugin/assets/dummy.png" alt="sdfdf" width="231" height="62" data-lazyload="assets/rs-plugin/assets/3e.jpg" data-no-retina> 
						</div>
						<!-- LAYER NR. 6 -->
						<div class="tp-caption Raleway   tp-resizeme" id="slide-41-layer-6" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1500;e:Power3.easeInOut;" data-transform_out="y:top;s:300;s:300;" data-start="250" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-captionhidden="on" style="z-index: 10; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);">
							<div style="text-align:center;">100% BEAUTIFULLY<br>
								<strong>RESPONSIVE</strong>
							</div>
						</div>
						<!-- LAYER NR. 7 -->
						<div class="tp-caption Raleway_small   tp-resizeme" id="slide-41-layer-7" data-x="center" data-hoffset="0" data-y="center" data-voffset="180" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:2000;e:Power4.easeInOut;" data-transform_out="y:top;s:300;s:300;" data-start="250" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 11; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);">
							<div style="text-align: center;">We kept it simple, while considering each scenario<br>
								so now it can handle any kind of content and still look great!
							</div> 
						</div>
					</li>
				</ul>
				<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	
			</div>
		</div><!-- END REVOLUTION SLIDER -->
	</section>

	<section class="spiral_section_tc section_with_header">
		<header>
			<div class="spiral_container">
				<h3>Welcome to <strong>FrappeDev</strong>. Leading Team in Web Development</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span3 spiral-animo" data-animation="fadeInUp" data-trigger_pt="0" data-duration="1500" data-delay="1000">
					<span class="clear spacer_11"></span>
					<h2 class="gray_text">
						<span>We take great<br> pride in our<br> achievements and<br> values</span>
					</h2>
					<span class="clear spacer_38"></span>
					<h4>
						<span>
							<a class="scroll" href="#">
								<strong>Learn more about us   </strong>
								<i class="tmf-arrow-down black_icon_down"></i>
							</a>
						</span>
					</h4>
				</div>
				<div class="spiral_column_tc_span3 spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration="2000" data-delay="0">
					<div class="spiral_service_box spiral_service_box_default ">
						<div class="spiral_service_box_header">
							<a href="#" target="_self" class="spiral_icon_boxed">
								<i class="tmf-cup service_box_default_icon_style"></i>
							</a>
							<a href="#" target="_self">
								<h3>CLIENTS</h3>
							</a>
						</div>
						<p>rem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</p>
						<div class="right_aligned learn_more">
							<p>
								<a href="#">Learn more 
									<i class="ci_icon-arrow-right"></i>
								</a>
							</p>
						</div>
					</div>
				</div>
				<div class="spiral_column_tc_span3 spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration="2000" data-delay="100">
					<div class="spiral_service_box spiral_service_box_default ">
						<div class="spiral_service_box_header">
							<a href="#" target="_self" class="spiral_icon_boxed">
								<i class="tmf-thought service_box_default_icon_style"></i>
							</a>
							<a href="#" target="_self"><h3>R&D</h3></a>
						</div>
						<p>is aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
						<p>Excepteur sint occaecat cupidatat non proident.</p>
						<div class="right_aligned learn_more">
							<p>
								<a href="#">Learn more <i class="ci_icon-arrow-right"></i></a>
							</p>
						</div>
					</div>
				</div>
				<div class="spiral_column_tc_span3 spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration="2000" data-delay="200">
					<div class="spiral_service_box spiral_service_box_default ">
						<div class="spiral_service_box_header">
							<a href="#" target="_self" class="spiral_icon_boxed">
								<i class="tmf-ruler-pencil service_box_default_icon_style"></i>
							</a>
							<a href="#" target="_self"><h3>PORTFOLIO</h3></a>
						</div>
						<p>cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.<br>
							Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium
						</p>
						<div class="right_aligned learn_more">
							<p>
								<a href="#">Learn more 
									<i class="ci_icon-arrow-right"></i>
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="spiral_section_tc spiral-inversed_text section_body_fullwidth section_with_header section_bg">
		<header>
			<div class="spiral_container">
				<h3>Featured 
					<strong>Projects</strong>
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12 center_aligned spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration="1000" data-delay="0">
					<div class="spiral_featured_portfolio clearfix">
						<div class="portfolio_item featured_item  design illustrations" data-post_id="85" data-name="Retro Ice Cream – Gallery" data-thumbnail='&lt;img src="assets/images/portfolio1.jpg" alt="portfolio1" /&gt;' data-image_link="assets/images/portfolio1.jpg" data-link_to="single-portfolio-gallery.html" data-cat="Design, Illustrations" data-date="15 June 2015" data-skills="Drawing, Illustrator, Photoshop" data-client="Company" data-description="Web design encompasses many different skills and disciplines in the production and maintenance of websites. Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines. Computer illustration or digital illustration is the use of digital tools to produce images under the direct manipulation of the artist, usually through a pointing device such as a tablet or a mouse. It is distinguished from computer-generated art, which is produced by a computer using mathematical models created by the artist." data-number="1">
							<div class="overlayed">
								<img src="assets/images/portfolio1.jpg" alt="portfolio1">
								<div class="overlay">
									<div class="portfolio_icons_container">
										<a class="portfolio_icon" href="single-portfolio-gallery.html">
											<i class="ci_icon-new-window"></i>
										</a>
									</div>
									<p class="overlay_title">Retro Ice Cream – Gallery</p>
									<p class="portfolio_item_tags">
										Design, Illustrations
									</p>
								</div>
							</div>
						</div>
						<div class="portfolio_item featured_item  illustrations" data-post_id="84" data-name="Retro Futuristic Car" data-thumbnail='&lt;img src="assets/images/portfolio8.jpg" alt="portfolio8" /&gt;' data-image_link="assets/images/portfolio8.jpg" data-link_to="single-portfolio-gallery.html" data-cat="Illustrations" data-date="15 June 2015" data-skills="Drawing, Illustrator, Photoshop" data-client="Company" data-description="Web design encompasses many different skills and disciplines in the production and maintenance of websites. Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines. Computer illustration or digital illustration is the use of digital tools to produce images under the direct manipulation of the artist, usually through a pointing device such as a tablet or a mouse. It is distinguished from computer-generated art, which is produced by a computer using mathematical models created by the artist." data-number="2">
							<div class="overlayed">
								<img src="assets/images/portfolio8.jpg" alt="portfolio8">
								<div class="overlay">
									<div class="portfolio_icons_container">
										<a class="portfolio_icon" href="single-portfolio-gallery.html">
											<i class="ci_icon-new-window"></i>
										</a>
									</div>
									<p class="overlay_title">Retro Futuristic Car</p>
									<p class="portfolio_item_tags">
										Illustrations
									</p>
								</div>
							</div>
						</div>
						<div class="portfolio_item featured_item  3d design" data-post_id="83" data-name="Ram Character" data-thumbnail='&lt;img src="assets/images/portfolio5.jpg" alt="portfolio5" /&gt;' data-image_link="assets/images/portfolio5.jpg" data-link_to="single-portfolio-gallery.html" data-cat="3D, Design" data-date="15 June 2015" data-skills="Drawing, Illustrator, Photoshop" data-client="Company" data-description="Web design encompasses many different skills and disciplines in the production and maintenance of websites. Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines. Computer illustration or digital illustration is the use of digital tools to produce images under the direct manipulation of the artist, usually through a pointing device such as a tablet or a mouse. It is distinguished from computer-generated art, which is produced by a computer using mathematical models created by the artist." data-number="3">
							<div class="overlayed">
								<img src="assets/images/portfolio5.jpg" alt="portfolio5">
								<div class="overlay">
									<div class="portfolio_icons_container">
										<a class="portfolio_icon" href="single-portfolio-gallery.html">
											<i class="ci_icon-new-window"></i>
										</a>
									</div>
									<p class="overlay_title">Ram Character</p>
									<p class="portfolio_item_tags">
										3D, Design
									</p>
								</div>
							</div>
						</div>
						<div class="portfolio_item featured_item  3d design" data-post_id="82" data-name="Oldman Character" data-thumbnail='&lt;img src="assets/images/portfolio4.jpg" alt="portfolio4" /&gt;' data-image_link="assets/images/portfolio4.jpg" data-link_to="single-portfolio-gallery.html" data-cat="3D, Design" data-date="15 June 2015" data-skills="Drawing, Illustrator, Photoshop" data-client="Company" data-description="Web design encompasses many different skills and disciplines in the production and maintenance of websites. Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines. Computer illustration or digital illustration is the use of digital tools to produce images under the direct manipulation of the artist, usually through a pointing device such as a tablet or a mouse. It is distinguished from computer-generated art, which is produced by a computer using mathematical models created by the artist." data-number="4">
							<div class="overlayed">
								<img src="assets/images/portfolio4.jpg" alt="portfolio4">
								<div class="overlay">
									<div class="portfolio_icons_container">
										<a class="portfolio_icon" href="single-portfolio-gallery.html">
											<i class="ci_icon-new-window"></i>
										</a>
									</div>
									<p class="overlay_title">Oldman Character</p>
									<p class="portfolio_item_tags">
										3D, Design
									</p>
								</div>
							</div>
						</div>
						<div class="portfolio_item featured_item  3d" data-post_id="79" data-name="Nature Hunter" data-thumbnail='&lt;img src="assets/images/portfolio13.jpg" alt="portfolio13" /&gt;' data-image_link="assets/images/portfolio13.jpg" data-link_to="single-portfolio-gallery.html" data-cat="3D" data-date="15 June 2015" data-skills="Drawing, Illustrator, Photoshop" data-client="Company" data-description="Web design encompasses many different skills and disciplines in the production and maintenance of websites. Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines. Computer illustration or digital illustration is the use of digital tools to produce images under the direct manipulation of the artist, usually through a pointing device such as a tablet or a mouse. It is distinguished from computer-generated art, which is produced by a computer using mathematical models created by the artist." data-number="5">
							<div class="overlayed">
								<img src="assets/images/portfolio13.jpg" alt="portfolio13">
								<div class="overlay">
									<div class="portfolio_icons_container">
										<a class="portfolio_icon" href="single-portfolio-gallery.html">
											<i class="ci_icon-new-window"></i>
										</a>
									</div>
									<p class="overlay_title">Nature Hunter</p>
									<p class="portfolio_item_tags">
										3D
									</p>
								</div>
							</div>
						</div>
						<div class="portfolio_item featured_item  3d design" data-post_id="77" data-name="Mermaid" data-thumbnail='&lt;img src="assets/images/portfolio14.jpg" alt="portfolio14" /&gt;' data-image_link="assets/images/portfolio14.jpg" data-link_to="single-portfolio-gallery.html" data-cat="3D, Design" data-date="15 June 2015" data-skills="Drawing, Illustrator, Photoshop" data-client="Company" data-description="Web design encompasses many different skills and disciplines in the production and maintenance of websites. Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines. Computer illustration or digital illustration is the use of digital tools to produce images under the direct manipulation of the artist, usually through a pointing device such as a tablet or a mouse. It is distinguished from computer-generated art, which is produced by a computer using mathematical models created by the artist." data-number="6">
							<div class="overlayed">
								<img src="assets/images/portfolio14.jpg" alt="portfolio14">
								<div class="overlay">
									<div class="portfolio_icons_container">
										<a class="portfolio_icon" href="single-portfolio-gallery.html">
											<i class="ci_icon-new-window"></i>
										</a>
									</div>
									<p class="overlay_title">Mermaid</p>
									<p class="portfolio_item_tags">
										3D, Design
									</p>
								</div>
							</div>
						</div>
						<div class="portfolio_item featured_item  design illustrations" data-post_id="75" data-name="Low Poly Tiger" data-thumbnail='&lt;img src="assets/images/portfolio9.jpg" alt="portfolio9" /&gt;' data-image_link="assets/images/portfolio9.jpg" data-link_to="single-portfolio-gallery.html" data-cat="Design, Illustrations" data-date="15 June 2015" data-skills="Drawing, Illustrator, Photoshop" data-client="Company" data-description="Web design encompasses many different skills and disciplines in the production and maintenance of websites. Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines. Computer illustration or digital illustration is the use of digital tools to produce images under the direct manipulation of the artist, usually through a pointing device such as a tablet or a mouse. It is distinguished from computer-generated art, which is produced by a computer using mathematical models created by the artist." data-number="7">
							<div class="overlayed">
								<img src="assets/images/portfolio9.jpg" alt="portfolio9">
								<div class="overlay">
									<div class="portfolio_icons_container">
										<a class="portfolio_icon" href="single-portfolio-gallery.html">
											<i class="ci_icon-new-window"></i>
										</a>
									</div>
									<p class="overlay_title">Low Poly Tiger</p>
									<p class="portfolio_item_tags">
										Design, Illustrations
									</p>
								</div>
							</div>
						</div>
						<div class="portfolio_item featured_item  illustrations" data-post_id="73" data-name="Like a Sir" data-thumbnail='&lt;img src="assets/images/portfolio16.jpg" alt="portfolio16" /&gt;' data-image_link="assets/images/portfolio16.jpg" data-link_to="single-portfolio-gallery.html" data-cat="Illustrations" data-date="15 June 2015" data-skills="Drawing, Illustrator, Photoshop" data-client="Company" data-description="Web design encompasses many different skills and disciplines in the production and maintenance of websites. Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines. Computer illustration or digital illustration is the use of digital tools to produce images under the direct manipulation of the artist, usually through a pointing device such as a tablet or a mouse. It is distinguished from computer-generated art, which is produced by a computer using mathematical models created by the artist." data-number="8">
							<div class="overlayed">
								<img src="assets/images/portfolio16.jpg" alt="portfolio16">
								<div class="overlay">
									<div class="portfolio_icons_container">
										<a class="portfolio_icon" href="single-portfolio-gallery.html">
											<i class="ci_icon-new-window"></i>
										</a>
									</div>
									<p class="overlay_title">Like a Sir</p>
									<p class="portfolio_item_tags">
										Illustrations
									</p>
								</div>
							</div>
						</div>
					</div>
					<div id="portfolio_content_details">
						<div id="pointer">
						</div>
						<div class="featured_gallery_post_wrapper">
							<div id="close_wrapper">
								<i class="ci_icon-close"></i>
							</div>
							<div class="gallery_post_image">
								<a class="fancybox" data-fancybox-group="portfolio" href="">
								</a>
							</div>
							<div class="gallery_post_content">
								<div class="gallery_post_links">
									<div class="gallery_post_title">
										<a href=""></a>
									</div>
									<div class="gallery_post_linkto">
										<a href=""><i class="ci_icon-new-window"></i></a>
									</div>
								</div>
								<div class="gallery_post_date">
								</div>
								<div class="gallery_post_description">
									<p></p>
								</div>
								<div class="gallery_post_info">
									<div class="gallery_post_category">
										<span>Categories</span>
										<span class="cat_names"></span>
									</div>
									<div class="gallery_post_client">
										<span>Client</span>
										<span class="client_names"></span>
									</div>
									<div class="gallery_post_skill">
										<span>Skills</span>
										<span class="skills_names"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<span class="clear spacer_29"></span>
					<a href="#" target="_self" class="spiral-button spiral-button_light spiral-button_rounded spiral-button_medium ripplelink spiral-button_transparent ">View all Projects</a>
				</div>
			</div>
		</div>
	</section>

	<section class="spiral_section_tc spiral-parallax section_body_fullwidth section_bg_image" data-background_image="assets/images/typingbg.jpg" data-parallax="0.1">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12 center_aligned spiral-animo" data-animation="fadeInUp" data-trigger_pt="0" data-duration="500" data-delay="0">
					<div id="spiral-modal-1" class="spiral-modal">
						<div class="spiral-modal-button" data-button_id="1">
							<img src="assets/images/play.png" alt="image button">
						</div>
						<div class="spiral-modal-content-wrapper" id="spiral-modal_wrapper_1">
							<div class="spiral-modal-content " id="spiral-modal_content_1">
								<div class="spiral-videoWrapper-vimeo">
									<iframe src="http://player.vimeo.com/video/57005606?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=0&amp;loop=0&amp;color=ff503f" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								</div>
								<div class="spiral-modal-close"></div>
							</div>
						</div>
					</div>
					<span class="clear spacer_40"></span>
					<h3 class="white_text center_aligned big_text">
						<span>Cafted with love to technical<br> excellence and exceptional attention to detail.</span>
					</h3>
					<span class="clear spacer_7"></span>
				</div>
			</div>
		</div>
	</section>

	<section id="who_we_are" class="spiral_section_tc section_with_header no_padding_bottom">
		<header>
			<div class="spiral_container">
				<h3>Who <strong>We</strong>Are</h3>
			</div>
			</header>
			<div class="spiral_section_content">
				<div class="spiral_container">
					<div class="spiral_column_tc_span6">
						<h2 class="gray_text no_margin_top">
							<span>Eventually everything connects –<br> people, ideas, shapes. The quality<br> of the connections is the key to<br> quality per se.</span>
						</h2>
						<span class="clear spacer_25"></span>
						<div>
							<p>rem ipsum dolor sit amet, consectetur adipisicing elit. Minima, minus iure accusantium dolores, officiis eveniet architecto error placeat ratione incidunt?</p>
						</div>
						<div>
							<p>rem ipsum dolor sit amet, consectetur adipisicing elit. Minima, minus iure accusantium dolores, officiis eveniet architecto error placeat ratione incidunt?</p>
						</div>
					</div>
					<div class="spiral_column_tc_span6">
						<div class="spiral-accordion " data-expanded="1">
							<h3>Vision of Spiral</h3>
							<div class="spiral-accordion-body">
								rem ipsum dolor sit amet, consectetur adipisicing elit. Ab vel mollitia dignissimos cupiditate rerum facilis similique vero possimus, dolor, voluptatibus minus enim aspernatur. Vel, molestias aut.
							</div>
							<h3>Our Mission</h3>
							<div class="spiral-accordion-body">
								rem ipsum dolor sit amet, consectetur adipisicing elit. Ab vel mollitia dignissimos cupiditate rerum facilis similique vero possimus, dolor, voluptatibus minus enim aspernatur. Vel, molestias aut.
							</div>
							<h3>Strategic Goals</h3>
							<div class="spiral-accordion-body">
								rem ipsum dolor sit amet, consectetur adipisicing elit. Ab vel mollitia dignissimos cupiditate rerum facilis similique vero possimus, dolor, voluptatibus minus enim aspernatur. Vel, molestias aut.
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="spiral_section_tc">
			<div class="spiral_section_content">
				<div class="spiral_container">
					<div class="spiral_column_tc_span4 spiral-animo" data-animation="bounceInLeft" data-trigger_pt="0" data-duration="750" data-delay="200">
						<div class="spiral_team_member">
							<div class="spiral_overlayed">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">
									<img src="assets/images/team3.jpg" alt="Angus">
								</a>
								<span class="spiral_team_member_name">
									<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">Angus Cameron</a>
								</span>
								<span class="spiral_team_member_position">Head of Office</span>
								<p class="team_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
								<div class="spiral_social_links">
									<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n">
										<i class="ci_icon-twitter"></i>
									</a>
									<a href="#" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n">
										<i class="ci_icon-linkedin"></i>
									</a>
									<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n">
										<i class="ci_icon-facebook"></i>
									</a>
								</div>
							</div>
							<div class="spiral_team_member_modal">
								<div class="spiral_container">
									<div class="spiral_column_tc_span5">
										<img src="assets/images/team3.jpg" alt="Angus">
										<div class="spiral_social_links">
											<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n">
												<i class="ci_icon-twitter"></i>
											</a>
											<a href="#" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n">
												<i class="ci_icon-linkedin"></i>
											</a>
											<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n">
												<i class="ci_icon-facebook"></i>
											</a>
										</div>
									</div>
									<div class="modal_line">
									</div>
									<div class="spiral_column_tc_span7">
										<h4 class="spiral_team_member_name">
											<span class="first_name">Angus</span>
											<span class="last_name">Cameron</span>
										</h4>
										<p class="spiral_team_member_position">Head of Office</p>
										<div class="content"></div>
									</div>
								</div>
								<div class="spiral_team_member_modal_close"></div>
							</div>
						</div>
					</div>
					<div class="spiral_column_tc_span4 spiral-animo" data-animation="bounceInLeft" data-trigger_pt="0" data-duration="850" data-delay="100">
						<div class="spiral_team_member">
							<div class="spiral_overlayed">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">
									<img src="assets/images/team2.jpg" alt="Mary M.">
								</a>
								<span class="spiral_team_member_name">
									<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">Mary M. Smith</a>
								</span>
								<span class="spiral_team_member_position">Designer</span>
								<p class="team_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
								<div class="spiral_social_links">
									<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n">
										<i class="ci_icon-twitter"></i>
									</a>
									<a href="#" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n">
										<i class="ci_icon-linkedin"></i>
									</a>
									<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n">
										<i class="ci_icon-facebook"></i>
									</a>
								</div>
							</div>
							<div class="spiral_team_member_modal">
								<div class="spiral_container">
									<div class="spiral_column_tc_span5">
										<img src="assets/images/team2.jpg" alt="Mary M.">
										<div class="spiral_social_links">
											<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n">
												<i class="ci_icon-twitter"></i>
											</a>
											<a href="#" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n">
												<i class="ci_icon-linkedin"></i>
											</a>
											<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n">
												<i class="ci_icon-facebook"></i>
											</a>
										</div>
									</div>
								<div class="modal_line"></div>
								<div class="spiral_column_tc_span7">
									<h4 class="spiral_team_member_name">
										<span class="first_name">Mary M.</span>
										<span class="last_name">Smith</span>
									</h4>
									<p class="spiral_team_member_position">Designer</p>
									<div class="content"></div>
								</div>
							</div>
							<div class="spiral_team_member_modal_close"></div>
						</div>
					</div>
				</div>
				<div class="spiral_column_tc_span4 spiral-animo" data-animation="bounceInLeft" data-trigger_pt="0" data-duration="950" data-delay="0">
					<div class="spiral_team_member">
						<div class="spiral_overlayed">
							<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">
								<img src="assets/images/team1.jpg" alt="Samuel">
							</a>
							<span class="spiral_team_member_name">
								<a class="spiral_team_member_link spiral_team_member_modal_link" href="#">Samuel Gilmore</a>
							</span>
							<span class="spiral_team_member_position">Public Relations</span>
							<p class="team_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<div class="spiral_social_links">
								<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n">
									<i class="ci_icon-twitter"></i>
								</a>
								<a href="#" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n">
									<i class="ci_icon-linkedin"></i>
								</a>
								<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n">
									<i class="ci_icon-facebook"></i>
								</a>
								<a href="#" target="_self" class="spiral_tooltip" title="Vimeo" data-gravity="n">
									<i class="ci_icon-vimeo"></i>
								</a>
							</div>
						</div>
						<div class="spiral_team_member_modal">
							<div class="spiral_container">
								<div class="spiral_column_tc_span5">
									<img src="assets/images/team1.jpg" alt="Samuel">
									<div class="spiral_social_links">
										<a href="#" target="_self" class="spiral_tooltip" title="Twitter" data-gravity="n">
											<i class="ci_icon-twitter"></i>
										</a>
										<a href="#" target="_self" class="spiral_tooltip" title="Linkedin" data-gravity="n">
											<i class="ci_icon-linkedin"></i>
										</a>
										<a href="#" target="_self" class="spiral_tooltip" title="Facebook" data-gravity="n">
											<i class="ci_icon-facebook"></i>
										</a>
										<a href="#" target="_self" class="spiral_tooltip" title="Vimeo" data-gravity="n">
											<i class="ci_icon-vimeo"></i>
										</a>
									</div>
								</div>
								<div class="modal_line"></div>
								<div class="spiral_column_tc_span7">
									<h4 class="spiral_team_member_name">
										<span class="first_name">Samuel</span>
										<span class="last_name">Gilmore</span>
									</h4>
									<p class="spiral_team_member_position">Public Relations</p>
									<div class="content"></div>
								</div>
							</div>
							<div class="spiral_team_member_modal_close"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="spiral_section_tc tabs_bg_black">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span4">
					<h3 class="white_text big_text">
						<span> 
							<strong>Milestones</strong>
							
						</span>
					</h3>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral_stats_excerpt  spiral_stats_excerpt_1">
						<i class="tmf-medall-alt stats_excerpt_1_color"></i>
						<span class="spiral_stats_number stats_number_color_white" data-number="74" data-duration="1500"></span>
						<p class="stats_number_color_white">Awards won</p>
					</div>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral_stats_excerpt  spiral_stats_excerpt_1">
						<i class="tmf-game stats_excerpt_1_color"></i>
						<span class="spiral_stats_number stats_number_color_white" data-number="3" data-duration="1500"></span>
						<p class="stats_number_color_white">Serious Gamers</p>
					</div>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral_stats_excerpt  spiral_stats_excerpt_1">
						<i class="tmf-receipt stats_excerpt_1_color"></i>
						<span class="spiral_stats_number stats_number_color_white" data-number="96" data-duration="1500"></span>
						<span class="spiral_stats_number_sign stats_number_color_white">k</span>
						<p class="stats_number_color_white">Lines of Code</p>
					</div>
				</div>
				<div class="spiral_column_tc_span2">
					<div class="spiral_stats_excerpt  spiral_stats_excerpt_1">
						<i class="tmf-package stats_excerpt_1_color"></i>
						<span class="spiral_stats_number stats_number_color_white" data-number="9" data-duration="1500"></span>
						<p class="stats_number_color_white">Projects Done</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="spiral_section_tc column_title_left section_with_header">
		<header>
			<div class="spiral_container">
				<h3>But don’t take our 
					<strong>Word</strong>
					for it
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span5">
					<div class="spiralt_testimonials_wrapper  picture_bottom">
						<ul class="spiralt_testimonials_slide" data-play="1" data-fx="fade" data-easing="linear" data-direction="left" data-duration="1000" data-pauseonhover="immediate" data-timeoutduration="5000">
							<li class="testimonials_item">
								<div class="testimonial_small">
									<p>Love the functionality of the theme, it's very versatile, Try it out, you won't regret it.</p>
									<img src="assets/images/testim1.jpg" alt="testim1">
									<span class="source">
										<span class="spiralt_author">Octavio Readmay</span>
										<span class="spiralt_company">Lolipop Inc.</span>
									</span>
								</div>
							</li>
							<li class="testimonials_item">
								<div class="testimonial_small">
									<p>There are a lot of cool features in this template. You should definitely buy it and check them out!</p>
									<img src="assets/images/testim2.jpg" alt="testim2">
									<span class="source">
										<span class="spiralt_author">Emmaline Blust</span>
										<span class="spiralt_company">Hightech Enterprise</span>
									</span>
								</div>
							</li>
							<li class="testimonials_item">
								<div class="testimonial_small">
									<p>Writing something instead of nothing is a good way to get noticed. Even in the simplest things.</p>
									<img src="assets/images/team21.jpg" alt="team2">
									<span class="source">
										<span class="spiralt_author">Rufus Huppert</span>
										<span class="spiralt_company">Happy Inc.</span>
									</span>
								</div>
							</li>
							<li class="testimonials_item">
								<div class="testimonial_small">
									<p>Good luck and keep it up! I have never seen such a great quality. Now I’m going to rate this item 5 stars. Because it’s awesome.</p>
									<img src="assets/images/testim4.jpg" alt="testim4">
									<span class="source">
										<span class="spiralt_author">Gena Brehmer</span>
										<span class="spiralt_company">Company Ltd.</span>
									</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="spiral_column_tc_span1 responsive_hide">
				</div>
				<div class="spiral_column_tc_span2 spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration="1000" data-delay="0">
					<div class="spiral-animo no_hover " data-animation="none" data-duration="1000" data-delay="0">
						<a href="#">
							<img src="assets/images/1.png" alt="">
						</a>
					</div>
					<span class="clear spacer_30">
					</span>
					<div class="spiral-animo no_hover " data-animation="none" data-duration="1000" data-delay="0">
						<a href="#">
							<img src="assets/images/4.png" alt="">
						</a>
					</div>
				</div>
				<div class="spiral_column_tc_span2 spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration="1000" data-delay="200">
					<div class="spiral-animo no_hover " data-animation="none" data-duration="1000" data-delay="0">
						<a href="#">
							<img src="assets/images/2.png" alt="">
						</a>
					</div>
					<span class="clear spacer_30">
					</span>
					<div class="spiral-animo no_hover " data-animation="none" data-duration="1000" data-delay="0">
						<a href="#">
							<img src="assets/images/5.png" alt="">
						</a>
					</div>
				</div>
				<div class="spiral_column_tc_span2 spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration="1000" data-delay="400">
					<div class="spiral-animo no_hover " data-animation="none" data-duration="1000" data-delay="0">
						<a href="#">
							<img src="assets/images/3.png" alt="">
						</a>
					</div>
					<span class="clear spacer_30">
					</span>
					<div class="spiral-animo no_hover " data-animation="none" data-duration="1000" data-delay="0">
						<a href="#">
							<img src="assets/images/6.png" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="spiral_section_tc spiral-centered spiral-inversed_text section_body_fullwidth section_with_header section_bg no_padding_bottom">
		<header>
			<div class="spiral_container">
				<h3>Latest 
					<strong>Blog</strong>
					News
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12 spiral-animo" data-animation="bounceInRight" data-trigger_pt="0" data-duration="1000" data-delay="0">
					<div class="spiral_post_excerpt_carousel" data-duration="800">
						<div class="carousel_navigation">
							<a href="#" class="carousel_prev">
								<i class="ci_icon-angle-left"></i>
							</a>
							<a href="#" class="carousel_next">
								<i class="ci_icon-angle-right"></i>
							</a>
						</div>
						<ul class="clearfix">
							<li class="first">
								<li>
									<div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix">
										<a class="spiral_latest_news_shortcode_thumb" href="#">
											<img src="assets/images/post2.jpg" alt="post2">
										</a>
										<div class="spiral_latest_news_shortcode_container">
											<div class="date_container">
												<div class="spiral_posts_date_month">Jun</div>
												<div class="spiral_posts_date_day">15</div>
											</div>
											<div class="spiral_latest_news_shortcode_content">
												<h5>
													<a href="#">This is an awesome title of blog post</a>
												</h5>
												<div class="spiral_posts_author">by 
													<a href="#">admin</a>
												</div>
												<div class="spiral_posts_comments">1 comment</div>
												<p>Proin gravida nibh vel velit auctor aliquet. Now that we know who you are, I know who I am. I’m...</p>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix">
										<a class="spiral_latest_news_shortcode_thumb" href="#">
											<img src="assets/images/post5.jpg" alt="post5">
										</a>
										<div class="spiral_latest_news_shortcode_container">
											<div class="date_container">
												<div class="spiral_posts_date_month">Jun</div>
												<div class="spiral_posts_date_day">15</div>
											</div>
											<div class="spiral_latest_news_shortcode_content">
												<h5>
													<a href="#">Post with youtube video</a>
												</h5>
												<div class="spiral_posts_author">by 
													<a href="#">admin</a>
												</div>
												<div class="spiral_posts_comments">0 comments</div>
												<p>This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Now that we know who you...</p>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix">
										<a class="spiral_latest_news_shortcode_thumb" href="#">
											<img src="assets/images/post1.jpg" alt="post1">
										</a>
										<div class="spiral_latest_news_shortcode_container">
											<div class="date_container">
												<div class="spiral_posts_date_month">Jun</div>
												<div class="spiral_posts_date_day">10</div>
											</div>
											<div class="spiral_latest_news_shortcode_content">
												<h5>
													<a href="#">Classic blog post</a>
												</h5>
												<div class="spiral_posts_author">by 
													<a href="#">admin</a>
												</div>
												<div class="spiral_posts_comments">0 comments</div>
												<p>This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Now that we know who you...</p>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix without_thumbnail">
										<div class="spiral_latest_news_shortcode_container">
											<div class="date_container">
												<div class="spiral_posts_date_month">Mar</div>
												<div class="spiral_posts_date_day">14</div>
											</div>
											<div class="spiral_latest_news_shortcode_content">
												<h5>
													<a href="#">Regular post without image</a>
												</h5>
												<div class="spiral_posts_author">by 
													<a href="#">admin</a>
												</div>
												<div class="spiral_posts_comments">0 comments</div>
												<p>This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Now that we know who you...</p>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix without_thumbnail">
										<div class="spiral_latest_news_shortcode_container">
											<div class="date_container">
												<div class="spiral_posts_date_month">Feb</div>
												<div class="spiral_posts_date_day">15</div>
											</div>
											<div class="spiral_latest_news_shortcode_content">
												<h5>
													<a href="#">Another cool Vimeo video</a>
												</h5>
												<div class="spiral_posts_author">by 
													<a href="#">admin</a>
												</div>
												<div class="spiral_posts_comments">0 comments</div>
												<p>Your bones don’t break, mine do. Proin gravida nibh vel velit auctor aliquet. Now that we know who you are,...</p>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix">
										<a class="spiral_latest_news_shortcode_thumb" href="#">
											<img src="assets/images/audiothumb1.jpg" alt="audiothumb1">
										</a>
										<div class="spiral_latest_news_shortcode_container">
											<div class="date_container">
												<div class="spiral_posts_date_month">Jan</div>
												<div class="spiral_posts_date_day">15</div>
											</div>
											<div class="spiral_latest_news_shortcode_content">
												<h5>
													<a href="#">Audio post example</a>
												</h5>
												<div class="spiral_posts_author">by 
													<a href="#">admin</a>
												</div>
												<div class="spiral_posts_comments">0 comments</div>
												<p>Proin gravida nibh vel velit auctor aliquet. Now that we know who you are, I know who I am. I’m...</p>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix">
										<a class="spiral_latest_news_shortcode_thumb" href="#">
											<img src="assets/images/post3.jpg" alt="post3">
										</a>
										<div class="spiral_latest_news_shortcode_container">
											<div class="date_container">
												<div class="spiral_posts_date_month">Nov</div>
												<div class="spiral_posts_date_day">15</div>
											</div>
											<div class="spiral_latest_news_shortcode_content">
												<h5>
													<a href="#">So you want to know more?</a>
												</h5>
												<div class="spiral_posts_author">by 
													<a href="#">admin</a>
												</div>
												<div class="spiral_posts_comments">0 comments</div>
												<p>This is Photoshop’s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Now that we know who you...</p>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix without_thumbnail">
										<div class="spiral_latest_news_shortcode_container">
											<div class="date_container">
												<div class="spiral_posts_date_month">Oct</div>
												<div class="spiral_posts_date_day">28</div>
											</div>
											<div class="spiral_latest_news_shortcode_content">
												<h5>
													<a href="#">Regular plog post</a>
												</h5>
												<div class="spiral_posts_author">by 
													<a href="#">admin</a>
												</div>
												<div class="spiral_posts_comments">0 comments</div>
												<p>Your bones don’t break, mine do. Proin gravida nibh vel velit auctor aliquet. Now that we know who you are,...</p>
											</div>
										</div>
									</div>
								</li>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="spiral_section_tc spiral-centered no_padding_top section_bg">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<a href="#" target="_self" class="spiral-button spiral-button_light spiral-button_rounded spiral-button_medium ripplelink spiral-button_transparent ">Visit Blog</a>
				</div>
			</div>
		</div>
	</section>

	<section class="spiral_section_tc column_title_left section_with_header">
		<header>
			<div class="spiral_container">
				<h3>Get in 
					<strong>Touch</strong>
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span4">
					<h3 class="gray_text">
						<span>Communication – the<br>human connection<br>– is the key to personal<br>and career success.”</span>
					</h3>
					<span class="clear spacer_30"></span>
					<div class="gray_text">
						<p>3285/2 first floor<br>New Tagore Nagar, Haibowal Kalan <br> Ludiana</p>
						<p>+91 161 504-7879<br>
							<a href="mailto:">info@frappedev.com</a>
						</p>
					</div>
				</div>
				<div class="spiral_column_tc_span8">
					<h2 class="dark_gray">
						<span>
							<strong>Say hello!</strong>
						</span>
					</h2>
					<span class="clear spacer_10"></span>
					<div class="spiralcf" id="spiralcf-wrapper" dir="ltr">
						<form action="#" method="post" class="contact-form">
							<div class="hidden">
								<input type="hidden" name="nonce" value="214a162653">
								<input type="hidden" name="formid" id="formid" value="contact">
							</div>
							<div class="row">
								<div class="span6">
									<span class="spiralcf-form-control-wrap your-name">
										<input type="text" name="name" value="" size="40" class="spiralcf-text" placeholder="Name">
									</span>
								</div>
								<div class="span6">
									<span class="spiralcf-form-control-wrap your-email">
										<input type="email" name="email" value="" size="40" class="spiralcf-text spiralcf-email spiralcf-validates-as-email" placeholder="Email">
									</span>
								</div>
							</div>
							<div class="row">
								<span class="spiralcf-form-control-wrap your-message">
									<textarea name="message" cols="40" rows="10" class="spiralcf-textarea" placeholder="Message"></textarea>
								</span>
								<br>
								<input type="submit" value="Send Message" class="spiralcf-submit"  id="spiralcf-submit">
							</div>
							<div class="spiralcf-response-output spiralcf-display-none"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="spiral_section_tc section_body_fullwidth no_padding">
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3422.813903172969!2d75.81710101474269!3d30.91982548157237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391a83efedfd8aed%3A0x2fee4423ea01682a!2s3285%2C+New+Tagore+Nagar%2C+Haibowal+Kalan%2C+Ludhiana%2C+Punjab+141001!5e0!3m2!1sen!2sin!4v1490773245655" width="1500" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						
					</div>
				</div>
			</div>
		</div>
	</section>

		<?php include('templates/footer.php'); ?>

	