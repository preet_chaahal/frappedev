<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Website extends CI_Controller {

	public function index()
	{
		$this->load->view('home');
	}

	public function about()
	{
		$this->load->view('about');
	}

	public function clients()
	{
		$this->load->view('clients');
	}

	public function portfolio()
	{
		$this->load->view('portfolio');
	}

	public function blog()
	{
		$this->load->view('blog');
	}
}